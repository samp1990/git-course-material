# Ensure that in Windows you are running Git Bash
# Continues in the wipeout repo

echo "new line of text" >> f1
git diff
git status
git checkout f1
git diff
git status
touch f2 f3 f4
echo "line of text" >> f2
git add f2
git status                # shows Staged: modified: f2; Untracked: f3, f4
git reset f2
git status                # shows Not staged: modifed: f2; Untracked: f3, f4
git checkout f2
git clean -n
git clean -f
