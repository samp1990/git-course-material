# Installation
  - use default options unless something different is required

# Options
  - Create new repo - creates a new local repo
  - Clone existing repo - makes exact copy of existing repo (including commits)
  - Open existing repo - as it says on the tin
  - Open recents - quick access to recently used repos

# Create new repo
  - Choose location
  - Click create
  - Check folder created in explorer - has hidden .git folder
  - Create a new file in explorer
  - Click Rescan in Git GUI - file created appears in Unstaged Changes
  - Click Stage Changed - file moves ti Staged Changes
  - Write a commit message in the window
  - Click Sign-off if required
  - Click Commit
  - File disappears as it has been added to the repo
  - Delete file in explorer
  - Rescan
  - Stage Changed
  - Write commit message and sign-off
  - Click Commit
