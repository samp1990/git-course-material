# Ensure that in Windows you are running Git Bash

mkdir gitDiff
cd gitDiff
git init

# Make the cache.manifest file with a line of text in it
echo "# Cache Manifest v1.1" > cache.manifest
git add cache.manifest
git commit -m "cache.manifest added"
echo "index.html" >> cache.manifest
git status
git diff
git add cache.manifest
git commit -m "index.html added to cache.manifest"
git status
