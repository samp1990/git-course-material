# Ensure that in Windows you are running Git Bash
# Continues in the wipeout repo

touch myfile.bat
git add myfile.bat
git commit

# In vi press 'i' to Insert text
myfile.bat added
# Press ESC then SHIFT+Z+Z to save and exit

git status
touch newfile.bat
add newfile.bat
git status
git commit -m "newfile.bat added"
git log --stat
